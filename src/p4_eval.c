#include <p4.h>

void			eval_diag_up(t_p4 *s_p4, int player)
{
   int		n;
   int		i;
   int		j;
   int		score;
   int		total;

   n = 0;
   total = 0;
   while (s_p4->nb_line > n)
   {
	  i = 0;
	  score = 0;
	  while ((s_p4->board[s_p4->nb_col * n + i] != player) 
			&& (i < s_p4->nb_col))
		 ++i;
	  j = n;
	  while ((s_p4->board[s_p4->nb_col * j + i] == player) 
			&& (i++ < s_p4->nb_col))
	  {
		 ++j;
		 ++score;
	  }
	  if (score == 1)
		 total += 1;
	  if (score == 2)
		 total += 20;
	  if (score == 3)
		 total += 500;
	  if (score == 4)
		 total += 2000;
	  if (player == PLAYER)
	  {
		 s_p4->score_player = s_p4->score_player < total ? total : s_p4->score_player;
		 s_p4->max_player = s_p4->max_player < score ? score : s_p4->max_player;
	  }
	  if (player == IA)
	  {
		 s_p4->score_ia = s_p4->score_ia < total ? total : s_p4->score_ia;
		 s_p4->max_ia = s_p4->max_ia < score ? score : s_p4->max_ia;
	  }
	  ++n;
   }
}

void			eval_diag_down(t_p4 *s_p4, int player)
{
   int		n;
   int		i;
   int		j;
   int		score;
   int		total;

   n = 0;
   total = 0;
   while (s_p4->nb_line > n)
   {
	  i = 0;
	  score = 0;
	  while ((s_p4->board[s_p4->nb_col * n + i] != player) 
			&& (i < s_p4->nb_col))
		 ++i;
	  j = n;
	  while ((s_p4->board[s_p4->nb_col * j + i] == player) 
			&& (i++ < s_p4->nb_col))
	  {
		 --j;
		 ++score;
	  }
	  if (score == 1)
		 total += 1;
	  if (score == 2)
		 total += 20;
	  if (score == 3)
		 total += 500;
	  if (score == 4)
		 total += 2000;
	  if (player == PLAYER)
	  {
		 s_p4->score_player = s_p4->score_player < total ? total : s_p4->score_player;
		 s_p4->max_player = s_p4->max_player < score ? score : s_p4->max_player;
	  }
	  if (player == IA)
	  {
		 s_p4->score_ia = s_p4->score_ia < total ? total : s_p4->score_ia;
		 s_p4->max_ia = s_p4->max_ia < score ? score : s_p4->max_ia;
	  }
	  ++n;
   }
}



