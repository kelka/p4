#include <p4.h>

void				print_board(t_p4 *s_p4)
{
   int				col;
   int				line;
   int				nb_space;

   nb_space = (s_p4->nb_col / 10) + 1;
   line = s_p4->nb_line - 1;
   while (line >= 0)
   {
	  col = 0;
	  while (col < s_p4->nb_col)
	  {
		 if (!(s_p4->board[col + (s_p4->nb_col * line)]))
			write(1, ".", 1);
		 else
			write(1, &(s_p4->board[col + (s_p4->nb_col * line)]), 1);
		 nb_space = (int)((col / 10) + 2);
		 while (nb_space--)
			ft_putstr(" ");
		 ++col;
	  }
	  write(1, "\n", 1);
	  --line;
   }
   col = 1;
   while (col <= (s_p4->nb_col))
   {
	  ft_putnbr(col); 
	  write(1, "  ", 2);
	  ++col;
   }
   write(1, "\n", 1);
}

int			add_piece(t_p4 *s_p4, int col, int player)
{
   int		line;
   int		ret;
   int		tmp;

   ret = -1;
   line = 0;
   while ((line < s_p4->nb_line) && (ret == -1))
   {
	  if (!(s_p4->board[line * s_p4->nb_col + col]))
	  {
		 ret = line;
		 s_p4->board[line * s_p4->nb_col + col] = player;
	  }
	  ++line;
   }
   return (ret);
}

void		rm_piece(t_p4 *s_p4, int col, int line, int player)
{
   if ((s_p4->board[line * s_p4->nb_col + col]) == player)
	  s_p4->board[line * s_p4->nb_col + col] = '\0';
}

int			read_user_move(t_p4 *s_p4)
{
   int		ret;
   char		*input;
   int		col;

   ret = -1;
   s_p4->user_move = -1;
   while (ret == -1)
   {
	  write(1, "What's your next move ? ", 24);
	  get_next_line(0, &input);
	  if ((ft_isnbr(input)) && ((col = ft_atoi(input)) > 0) 
			&& (col <= s_p4->nb_col))
	  {

		 s_p4->user_move = col - 1;
		 ret = add_piece(s_p4, col - 1, PLAYER);
		 s_p4->free_space--;
		 write(1, "\n", 1);
	  }
   }
   print_board(s_p4);
   eval_score(s_p4);
  if ((s_p4->free_space < 1) || (s_p4->max_player > 3) || (s_p4->max_ia > 3))
   {
	  print_score(s_p4);
	 return (1);
   }
  else
	 p4_ia(s_p4);
}

static int		who_plays_first(t_p4 *s_p4)
{
   int	player;

   srand(time(NULL));
   player = rand() % 2;
   if (player)
   {
	  print_board(s_p4);
	  read_user_move(s_p4);
   }
   else
	  p4_ia(s_p4);
   return (player);
}

int			start_p4_game(t_p4 *s_p4)
{
   int		ret;
   int		advance;

   ret = 0;
   advance = (s_p4->nb_col * s_p4->nb_line) > (12 * 12) ? 2 : 5;
   s_p4->free_space = s_p4->nb_col * s_p4->nb_line;
   s_p4->depth = ft_recursive_power(s_p4->nb_col, advance);
   who_plays_first(s_p4);
   return (ret);
}

