#include <p4.h>

static void			eval_line(t_p4 *s_p4, int player)
{
   int		n;
   int		i;
   int		score;
   int		total;

   n = 0;
   total = 0;
   while (s_p4->nb_line > n)
   {
	  i = 0;
	  score = 0;
	  while ((s_p4->board[s_p4->nb_col * n + i] != player) 
			&& (i < s_p4->nb_col))
		 ++i;
	  while ((s_p4->board[s_p4->nb_col * n + i] == player) 
			&& (i++ < s_p4->nb_col))
	  {
		 ++score;
	  }
	  if (score == 1)
		 total += 1;
	  if (score == 2)
		 total += 20;
	  if (score == 3)
		 total += 500;
	  if (score == 4)
		 total += 2000;
	  if (player == PLAYER)
	  {
		 s_p4->score_player = s_p4->score_player < total ? total : s_p4->score_player;
		 s_p4->max_player = s_p4->max_player < score ? score : s_p4->max_player;
	  }
	  if (player == IA)
	  {
		 s_p4->score_ia = s_p4->score_ia < total ? total : s_p4->score_ia;
		 s_p4->max_ia = s_p4->max_ia < score ? score : s_p4->max_ia;
	  }
	  ++n;
   }
}


static void			eval_column(t_p4 *s_p4, int player)
{
   int		n;
   int		i;
   int		score;
   int		free;
   int		total;

   n = 0;
   total = 0;
   while (s_p4->nb_col > n)
   {
	  i = 0;
	  score = 0;
	  while ((s_p4->board[s_p4->nb_col * i + n] == player) 
			&& (i++ < s_p4->nb_line))
	  {
		 ++score;
	  }
	  if (score == 1)
		 total += 1;
	  if (score == 2)
		 total += 20;
	  if (score == 3)
		 total += 500;
	  if (score == 4)
		 total += 3000;
	  if (player == PLAYER)
	  {
		 s_p4->score_player = s_p4->score_player < total ? total : s_p4->score_player;
		 s_p4->max_player = s_p4->max_player < score ? score : s_p4->max_player;
	  }
	  if (player == IA)
	  {
		 s_p4->score_ia = s_p4->score_ia < total ? total : s_p4->score_ia;
		 s_p4->max_ia = s_p4->max_ia < score ? score : s_p4->max_ia;
	  }
	  ++n;
   }
}

int			eval_score(t_p4 *s_p4)
{
   s_p4->max_player = INT_MIN;
   s_p4->max_ia = INT_MIN;
   s_p4->score_ia = INT_MIN;
   s_p4->score_player = INT_MIN;
   eval_column(s_p4, PLAYER);
   eval_line(s_p4, PLAYER);
   eval_diag_up(s_p4, PLAYER);
   eval_diag_down(s_p4, PLAYER);
   eval_column(s_p4, IA);
   eval_line(s_p4, IA);
   eval_diag_up(s_p4, IA);
   eval_diag_down(s_p4, IA);
   return (s_p4->score_ia - s_p4->score_player * 1.5);
}

static int		end_game(t_p4 *s_p4)
{
   s_p4->eval = eval_score(s_p4);
   if ((s_p4->free_space < 1) || (s_p4->max_ia > 3) || (s_p4->max_player > 3)
		 || (s_p4->depth < 1))
	  return (1);
   return (0);
}

int				p4_max(t_p4 *s_p4)
{
   int		max;
   int		tmp;
   int		n;
   int		line;

   max = INT_MIN;
   --s_p4->depth;
   if (end_game(s_p4))
	  return (s_p4->eval);
   n = -1;
    while (++n < (s_p4->nb_col - 1))
   {
	  line = add_piece(s_p4, n, IA);
	  s_p4->free_space--;
	  tmp = p4_min(s_p4);
	  max = max < tmp ? tmp : max;
	  s_p4->free_space++;
	  rm_piece(s_p4, n, line, IA);
   }
	return (max);
}

int				p4_min(t_p4 *s_p4)
{
   int		min;
   int		tmp;
   int		n;
   int		line;

   min = INT_MAX;
   --s_p4->depth;
   if (end_game(s_p4))
	  return (s_p4->eval);
   n = -1;
   while (++n < (s_p4->nb_col - 1))
   {
	  line = add_piece(s_p4, n, PLAYER);
	  s_p4->free_space--;
	  tmp = p4_max(s_p4);
	  min = min > tmp ? tmp : min;
	  s_p4->free_space++;
	  rm_piece(s_p4, n, line, PLAYER);
   }
   return (min);
}

void			print_score(t_p4 *s_p4)
{
   if ((s_p4->max_ia > 3) && (s_p4->max_player < 4))
	  write(1, "\nI Win\n", 7);
   else if ((s_p4->max_player > 3) && (s_p4->max_ia < 4))
	  write(1, "\nYou Win, congratz !\n", 21);
   else
	  write(1, "\nex-aequo\n", 10);
}

int			p4_ia(t_p4 *s_p4)
{
   int		max_val;
   int		n;
   int		tmp;
   int		line;

   n = -1;
   max_val = INT_MIN;
   while (++n < (s_p4->nb_col - 1))
   {
	  line = add_piece(s_p4, n, IA);
	  s_p4->free_space--;
	  tmp = p4_min(s_p4);
	  if (tmp > max_val)
	  {
		 max_val = tmp;
		 s_p4->ia_move = n;
	  }
	  rm_piece(s_p4, n, line, IA);	  
	  s_p4->free_space++;
   }
   line  = add_piece(s_p4, s_p4->ia_move, IA);
   s_p4->free_space--;
   eval_score(s_p4);
   print_board(s_p4);
   if ((s_p4->free_space < 1) || (s_p4->max_player > 3) || (s_p4->max_ia > 3))
   {
	  print_score(s_p4);
	  return (1);
   }
   else
	  read_user_move(s_p4);
}

