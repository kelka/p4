#include <get_next_line.h>

int				fill_buff(const int fd, char *buff, int line_len, char **line)
{
   int			read_len;
   char			*end_line;
   char			*tmp;
   int			ret;

   ret = ENOMEM;
   if ((tmp = ft_strnew(sizeof(*tmp) * BUFF_SIZE)))
   {
	  ret = 0;
	  if ((read_len = read(fd, tmp, BUFF_SIZE)))
	  {
		 ret = 1;
		 if (!(end_line = ft_memchr(tmp, '\n', BUFF_SIZE)))
		 {
			line_len += BUFF_SIZE;
			fill_buff(fd, buff, line_len, line);
		 }
		 else
		 {
			read_len = end_line - tmp;
			line_len += read_len;
			buff = ft_memcpy(buff, end_line + 1, BUFF_SIZE - (end_line - tmp) - 1); 
		 }
		 if (*line || (*line = ft_strnew(sizeof(**line) * (line_len + 1))))
		 {
			ft_memcpy((void *)(*line + line_len - read_len), tmp, read_len);
		 }
		 else
			return (ENOMEM);
	  }
	  free(tmp);
	  tmp = NULL;
   }
   return (ret);
}

int				fill_line(char *buff, char **line)
{
   char			*end_line;
   size_t		line_len;

   line_len = BUFF_SIZE;
   if ((end_line = ft_memchr(buff, '\n', BUFF_SIZE)))
   {
	  line_len = end_line - buff;
	  if (line_len)
	  {
		 if ((*line = ft_strnew(sizeof(**line) * line_len + 1)))
		 {
			*line = ft_memcpy(*line, buff, line_len);
			buff = ft_memcpy(buff, end_line + 1, BUFF_SIZE - line_len);
			ft_bzero((buff + line_len), BUFF_SIZE - line_len);
			return (1);
		 }
		 else
			return (ENOMEM);
	  }
   }
   return (0);
}

int				get_next_line(const int fd, char **line)
{
   static char		*buff = NULL;
   char				*tmp_buff;
   char				*tmp_line;
   int				ret;

   *line = NULL;
   ret = 0;
   if (!buff && !(buff = ft_strnew(sizeof(*buff) * BUFF_SIZE)))
   {
	  ret = ENOMEM;
   }
   else if (buff && *buff)
   {
	  ret = fill_line(buff, line);
   }
   if (ret == 0)
   {
	  tmp_buff = ft_strdup(buff);
	  ft_bzero(buff, BUFF_SIZE);
	  ret = fill_buff(fd, buff, 0, line);
	  tmp_line = *line;
	  *line = ft_strnew(sizeof(*line) * (BUFF_SIZE + ft_strlen(tmp_line) + 1));
	  ft_strcpy(*line, tmp_buff);
	  *line = ft_strncat(*line, tmp_line, ft_strlen(tmp_line));
	  free(tmp_buff);
	  free(tmp_line);
   }
   //ft_printf("line: *%s*\nbuff: *%s*\nret: %d\n\n", *line, buff, ret);
   if (ret < 1)
	  free(buff);
   return (ret);
}

