/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 02:06:12 by ccano             #+#    #+#             */
/*   Updated: 2013/12/01 15:18:55 by klo              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void		*ft_memset(void *dest, int pattern, size_t size)
{
	u_char		*dest_ptr;

	dest_ptr = (u_char *)dest;
	if (dest && size > 0)
	{
		while (size--)
			*dest_ptr++ = (u_char)pattern;
	}
	return (dest);
}

