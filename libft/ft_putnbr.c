/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 02:08:26 by ccano             #+#    #+#             */
/*   Updated: 2013/12/02 01:17:25 by klo              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int		ft_putnbr(int n)
{
	static char		res;
	int				ret;

	ret = 0;
	if (n < 0)
	{
		n = n * -1;
		write(1, "-", 1);
		++ret;
	}
	if (n > 9)
	{
		ft_putnbr(n / 10);
		ft_putnbr(n % 10);
	}
	else
	{
		res = 48 + n;
		write(1, &res, 1);
		++ret;
	}
	return (ret);
}

