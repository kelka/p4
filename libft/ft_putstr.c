/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 02:08:53 by ccano             #+#    #+#             */
/*   Updated: 2013/12/02 01:14:20 by klo              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int			ft_putstr(char const *str)
{
   int		str_len;

   str_len = 0;
   if (str)
   {
	  str_len = ft_strlen(str);
	  write(1, str, str_len);
   }
   return (str_len);
}

