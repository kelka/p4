#include <libft.h>

int				ft_isnbr(const char *s)
{
   int			ret;

   ret = 1;
   while (*s && (ret == 1))
   {
	  if (!(ft_isdigit((int)(*s))))
		 ret = 0;
	  ++s;
   }
   return (ret);
}

