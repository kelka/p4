/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: klo <klo.elka@gmail.com>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/17 01:43:01 by klo               #+#    #+#             */
/*   Updated: 2013/12/01 22:22:57 by klo              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strncat(char *dest, const char *src, size_t n)
{
   char	*dest_begin;

   dest_begin = NULL;
   if (dest && src)
   {
	  dest_begin = dest;
	  if (n)
	  {
		 while (*dest)
			++dest;
		 while (n > 0 && *src)
		 {
			*dest++ = *src++;
			--n;
		 }
		 *dest = '\0';
	  }
   }
   return (dest_begin);
}

