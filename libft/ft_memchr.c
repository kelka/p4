/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 02:05:33 by ccano             #+#    #+#             */
/*   Updated: 2013/12/02 02:34:55 by klo              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void			*ft_memchr(const void *src, int to_match, size_t size)
{
	u_char		*src_ptr;

	if (size && src)
	{
		src_ptr = (u_char *)src;
		while (size--)
		{
			if (*src_ptr == (u_char)to_match)
				return ((void *)src_ptr);
			src_ptr++;
		}
	}
	return (NULL);
}

