/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 02:02:52 by ccano             #+#    #+#             */
/*   Updated: 2013/11/24 11:42:20 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
   int	nbr;
   int	is_negative;

   nbr = 0;
   is_negative = -1;
   while (*str && ((*str == '\t') || (*str == '\n') || (*str == '\v') || 
		   (*str == '\f') || (*str == '\r') || (*str == ' ')))
	  ++str;
   
   if ((*str == '+') || (*str == '-'))
   {
	  if (*str == '-')
		 is_negative *= -1;
   ++str;
   }
   
   while ( *str && (*str >= '0') && (*str <= '9'))
	  nbr = nbr * 10 + *str++ - '0';
   
   if (is_negative == 1)
	  nbr = nbr * -1;
   
   return (nbr);
}

/* out of range values for an int causes undefined behavior
** (man atoi)												*/
