/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 04:23:39 by ccano             #+#    #+#             */
/*   Updated: 2013/12/02 04:39:40 by klo              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memalloc(size_t size)
{
	void		*buf;

	if (size < 1)
		return (NULL);
	if ((buf = malloc(size)) == NULL)
		return (NULL);
	ft_bzero(buf, size);
	return (buf);
}

