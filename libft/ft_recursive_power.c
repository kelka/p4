int		ft_recursive_power(int nbr, int pow)
{
	if (pow == 0)
		return (1);

	if (pow == 1)
		return (nbr);

	if (pow > 1)
	{
		return (nbr * ft_recursive_power(nbr, pow - 1));
	}
	return (0);
}
