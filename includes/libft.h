/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: klo <klo.elka@gmail.com>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/01 13:08:37 by klo               #+#    #+#             */
/*   Updated: 2014/01/04 16:55:12 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef LIBFT_H
# define LIBFT_H

# include <string.h>
# include <stdlib.h>
# include <unistd.h>

# define MASK_0 = 0x00

typedef unsigned char		u_char;
typedef unsigned int		u_int;


/*
** Undocumented functions are implementations of standard libc functions.
** Documentation about those functions can be found in Unix man.
*/


/*
** SET / ALLOC / FREE MEMORY
** ============================================================================
*/

void		*ft_memset(void *dest, int pattern, size_t size);
void		ft_bzero(void *dest, size_t size);

/*
** Allocate size bytes of memory with malloc, set them to 0 then
** return the adress of this new clean buffer, or NULL if memory allocation
** fail.
*/
void		*ft_memalloc(size_t size);
char		*ft_strnew(size_t size);

/*
** del: Free memory pointed to by *ap / *as and set *ap / *as to NULL.
** clr: free memory pointed to by s, set s to NULL.
*/
void		ft_memdel(void **ap);
void		ft_strdel(char **as);
void		ft_strclr(char *s);

/*
** COPY / CONCATENATE / DUPLICATE
** ============================================================================
*/

void		*ft_memcpy(void *dest, const void *src, size_t size);
void		*ft_memccpy(void *dest, const void *src, int c, size_t n);
void		*ft_memmove(void *dest, const void *src, size_t n);

char		*ft_strcpy(char *dest, const char *src);
char		*ft_strncpy(char *dest, const char *src, size_t n);
char		*ft_strdup(const char *s);

char		*ft_strcat(char *dest, const char *src);
char		*ft_strncat(char *dest, const char *src, size_t n);
size_t		ft_strlcat(char *dst, const char *src, size_t size);

/*
** Allocate with malloc(), concatenate s1 and s2 and put a final '\0'.
** Return this new string unless allocation failed, in which case NULL is
** returned.
*/
char		*ft_strjoin(char const *s1, char const *s2);

/*
** Trim whitespaces (' ', '\t' and '\n') at the beggining and the end of the
** string pointed to by s.
*/
char		*ft_strtrim(char const *s);

/*
** Allocate an array of clean strings with malloc() filling this array with
** substring resulting from the split of s each time c is met. Also, c is not
** copied in the new substrings. Return this array or NULL if malloc fail.
*/
char		**ft_strsplit(char const *s, char c);

/*
** Allocate a new string with malloc then copy len char of s starting at index
** start. If the substring delimited by start and len is not valid, comportement
** is indeterminated. Return he substring or NULL if allocation failed.
*/
char		*ft_strsub(char const *s, u_int start, size_t len);

char		*ft_strrev(char *s);

/*
** SEARCH / COMPARE
*/

void		*ft_memchr(const void *s, int c, size_t n);

char		*ft_strchr(const char *s, int c);
char		*ft_strrchr(const char *s, int c);
char		*ft_strstr(const char *haystack, const char *needle);
char		*ft_strnstr(const char *big, const char *little, size_t len);

int			ft_memcmp(const void *s1, const void *s2, size_t n);

# define COMP(x, y)		(x) - (y)

int			ft_strcmp(const char *s1, const char *s2);
int			ft_strncmp(const char *s1, const char *s2, size_t n);

/*
** Compare lexicographicaly s1 and s2. Return 1 if equal, else return 0.
** ft_strnequ(): Compare no more than size bytes.
*/
int			ft_strequ(char const *s1, char const *s2);
int			ft_strnequ(char const *s1, char const *s2, size_t n);


/*
** STRING LENGTH
** =============================================================================
*/

size_t		ft_strlen(const char *s);

/*
** PARSING OF INTEGERS
** =============================================================================
*/

int			ft_atoi(const char *nptr);
int			ft_antoi(const char *nptr, size_t n);
char		*ft_itoa(int n);


/*
** CLASSIFICATION OF CHARACTERS
** =============================================================================
*/

int			ft_isalnum(int c);
int			ft_isalpha(int c);
int			ft_isascii(int c);
int			ft_isdigit(int c);
int			ft_isprint(int c);
int			ft_isblank(int c);

int			ft_isnbr(const char *s);

int			ft_toupper(int c);
int			ft_tolower(int c);


/*
** FUNCTION POINTER
** =============================================================================
*/

/*
** ft_striteri(): at each char of the string.
** ft_striteri(): at each char of the string starting at the index given in
**				  first parameter of the function pointer.
*/
void		ft_striter(char *s, void (*f)(char *));
void		ft_striteri(char *s, void (*f)(u_int, char *));

/*
** Return a fresh string created by the successive application of f on each char
** of the string pointed to by s (starting at the index given in first parameter
** of s fot ft_strmapi()).
*/
char		*ft_strmap(char const *s, char (*f)(char));
char		*ft_strmapi(char const *s, char (*f)(u_int, char));


/*
** WRITE
** ============================================================================
*/

/*
** To stdout
*/
/* write a char to stdout*/
void		ft_putchar(const char c);
/* write a char to stdout, return the nb of written char(s) */
int			ft_putstr(char const *s);
/* write a char to stdout, return the nb of written char(s) */
void		ft_putendl(char const *s);
/* write a char to stdout*/
int			ft_putnbr(int n);
int			ft_putnbr_base(int n, char *base);

/*
** To a file
*/
void		ft_putchar_fd(char c, int fd);
void		ft_putstr_fd(char const *s, int fd);
void		ft_putendl_fd(char const *s, int fd);
void		ft_putnbr_fd(int n, int fd);

/*
** Colorized output
*/
# define ANSI_RED				"\x1b[31m"
# define ANSI_GRN				"\x1b[32m"
# define ANSI_YEL				"\x1b[33m"
# define ANSI_BLU				"\x1b[33m"
# define ANSI_MAJ				"\x1b[34m"
# define ANSI_CYA				"\x1b[35m"
# define ANSI_COLOR_RESET		"\x1b[0m"


/*
** LISTS
** =============================================================================
*/

typedef struct			s_list
{
	void			*content;
	size_t			content_size;
	void			*next;
}						t_list;

/*
** ALLOCATE with ft_memalloc. If content is NULL, content and content_size
** fields are set to NULL. Else, size bytes of content are copied in a newly
** allocated buffer, pointed to by content field, and content_size is set to
** size. Return the adress of this new t_list element or NULL if one of those
** allocation failed.
*/
t_list		*ft_lstnew(void const *content, size_t content_size);


/*
** lstdelone(): FREE the memory at alst->content with the del function and set
** alst to NULL.
** lstdel(): Apply del to all elem of the list from *alst, then set *alst to
** NULL (->thus erasing the list if *alst point the 1st element).
*/
void		ft_lstdelone(t_list **alst, void (*del)(void *));
void		ft_lstdel(t_list **alst, void (*del)(void *));

/*
** Return the SIZE (the number of element) of the list.
*/
int			ft_lstsize(t_list *begin_list);

/*
** PUSH a new elem back/front. If elem is NULL, *begin_list is not updated so we
** won't loose access to a list. If *begin_list is NULL, new become the first
** elem of the list.
*/
void		ft_lst_push_back(t_list **begin_list, t_list *new);
void		ft_lst_push_front(t_list **begin_list, t_list *new);

/*
** ENVIRONMENT
** ============================================================================
*/


extern char	**environ;

/*
** Get the name environment variable, then split it at each ':'. Return an
** array of the substings or NULL if name is not found in the global variable
** environ. getenvt return a char **.
*/
char		**ft_getenvt(char *name);
char		*ft_getenv(char *name);

int		ft_recursive_power(int nbr, int pow);

#endif /* LIBFT_H */

