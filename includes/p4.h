#ifndef P4_H
# define P4_H

# include <libft.h>
# include <get_next_line.h>
# include <time.h>
# include <limits.h>

# define PLAYER		'O'	
# define IA			'X'

typedef struct s_p4		t_p4;

struct					s_p4
{
   int			nb_col;
   int			nb_line;
   char			*board;
   int			user_move;
   int			ia_move;
   int			max_ia;
   int			max_player;
   int			score_ia;
   int			score_player;
   int			free_space;
   int			depth;
   int			eval;
};

int						start_p4_game(t_p4 *s_p4);
int						read_user_move(t_p4 *s_p4);
int						p4_ia(t_p4 *s_p4);
int						edit_board(t_p4 *s_p4, int col, int player, int add);
void					rm_piece(t_p4 *s_p4, int col, int line, int player);
int						add_piece(t_p4 *s_p4, int col, int player);
void					print_board(t_p4 *s_p4);
int						eval_score(t_p4 *s_p4);
void					print_score(t_p4 *s_p4);

int				p4_max(t_p4 *s_p4);
int				p4_min(t_p4 *s_p4);

void				eval_diag_up(t_p4 *s_p4, int player);
void				eval_diag_down(t_p4 *s_p4, int player);

#endif
