/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: klo <klo.elka@gmail.com>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/05 01:49:10 by klo               #+#    #+#             */
/*   Updated: 2013/12/07 02:39:16 by klo              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE
# define GET_NEXT_LINE

// # include <dirent.h>
//# include <sys/stat.h>
# include <fcntl.h>
# include <libft.h>

# define BUFF_SIZE	10 

# define ENOMEM		-2

int		get_next_line(const int fd, char **line);

#endif /* !GET_NEXT_LINE */

