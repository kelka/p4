NAME = puissance4
P4_SRC = ./src/main.c ./src/p4.c ./src/p4_ia.c ./src/p4_eval.c
LIB_SRC = ./libft/ft_atoi.c ./libft/ft_isdigit.c ./libft/ft_isnbr.c \
		  ./libft/ft_putstr.c ./libft/ft_strnew.c ./libft/ft_memalloc.c \
		  ./libft/ft_strlen.c ./libft/ft_bzero.c ./libft/ft_memset.c \
		  ./libft/ft_putnbr.c ./libft/get_next_line.c ./libft/ft_memchr.c \
		  ./libft/ft_strdup.c ./libft/ft_memcpy.c ./libft/ft_strncat.c \
		  ./libft/ft_strcpy.c ./libft/ft_recursive_power.c
P4_OBJ = $(P4_SRC:.c=.o)
LIB_OBJ = $(LIB_SRC:.c=.o)

all: $(NAME)

$(NAME):
	gcc -Wall -Wextra -o $(NAME) $(P4_SRC) $(LIB_SRC) -I./includes -ggdb3

clean:
	rm -rf $(P4_OBJ)
	rm -rf $(SRC_OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all

